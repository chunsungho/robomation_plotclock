import org.roboid.cheese.*;
import org.roboid.hamster.*;
import org.roboid.master.*;
import org.roboid.robot.*;
import org.roboid.runtime.*;

import controlP5.*;

Cheese cs;
Scara scara;

int mouseClick = 0;

void setup(){
  cs = new Cheese(this);
  scara = new Scara(cs);
  scara.Calibration(-4,47,75);  // 하드웨어 보정
  size(400,400);
  
  background(255);
  
  stroke(255,0,0);
  strokeWeight(1);
  line(100,0,100,400);
  line(200,0,200,400);
  line(300,0,300,400);
}

void draw(){
  
}



void loop(){
  
  switch(mouseClick)
  {
   case 1:
    // 글씨쓰기
    if(scara.writtenFlag == 0)  // 0개 썼으면
    {
      if(!scara.didJump)  // jump 안했으면
      {
       scara.Jump(1,1);  // 첫 번째칸, 1자리로 jump
      }else
      {
        scara.Draw(1,1);  // 첫 번째칸에 1 쓰기
      }
      
    }else if(scara.writtenFlag == 1)  // 1개 썼으면
    {
      if(!scara.didJump)  // jump 안했으면
      {
       scara.Jump(2,2);  // 두 번째칸, 2자리로 jump
      }else
      {
        scara.Draw(2,2);  // 두 번째칸에 2 쓰기
      }
    }else if(scara.writtenFlag == 2)  // 2개 썼으면
    {
      scara.DrawColon();      // ':' 쓰기
    }else if(scara.writtenFlag == 3)  // 3개 썼으면
    {
      if(!scara.didJump)  // jump 안했으면
      {
       scara.Jump(3,3);
      }else
      {
        scara.Draw(3,3);
      }
    }else if(scara.writtenFlag == 4)
    {
      if(!scara.didJump)
      {
       scara.Jump(4,7);
      }else
      {
        scara.Draw(4,7);
      }
    }else
    {
     scara.PenUp();
     mouseClick = 0;  // 초기화 안해주면 바로 다시 글씨쓴다.
    }
    
    break;
    
   case 2:  //erase all
    
    scara.EraseBoard();
    
    break;
    
   default:
     //none
    break; 
  }
}  //loop


void keyPressed() {
    if(key == 'q') 
    {
      mouseClick = 0;
      scara.ResetTic();
      scara.ResetEraser();
      scara.ResetPen();
    }else if(key == 'w')
    {
      mouseClick = 1;
    }else if(key == 'e') 
    {
      mouseClick = 2;
    }else
    {
      // None
    }
    
} // keyPressed()