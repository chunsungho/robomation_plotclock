
class Point
{
  protected float[][] pointsX = new float[10][50];
  protected float[][] pointsY = new float[10][50];

  Point()
  {
    //number : 0
    pointsX[0][0]=4.0; pointsY[0][0]=27.599998 ;
    pointsX[0][1]=4.2000003; pointsY[0][1]=27.599998 ;
    pointsX[0][2]=5.0; pointsY[0][2]=27.300003 ;
    pointsX[0][3]=5.8; pointsY[0][3]=27.099998 ;
    pointsX[0][4]=6.3; pointsY[0][4]=26.800003 ;
    pointsX[0][5]=6.8; pointsY[0][5]=26.300003 ;
    pointsX[0][6]=7.2000003; pointsY[0][6]=25.400002 ;
    pointsX[0][7]=7.5; pointsY[0][7]=24.300003 ;
    pointsX[0][8]=7.6; pointsY[0][8]=23.2 ;
    pointsX[0][9]=7.7000003; pointsY[0][9]=22.0 ;
    pointsX[0][10]=7.7000003; pointsY[0][10]=20.900002 ;
    pointsX[0][11]=7.9; pointsY[0][11]=19.5 ;
    pointsX[0][12]=8.0; pointsY[0][12]=18.300003 ;
    pointsX[0][13]=8.0; pointsY[0][13]=17.099998 ;
    pointsX[0][14]=7.9; pointsY[0][14]=15.700001 ;
    pointsX[0][15]=7.6; pointsY[0][15]=14.5 ;
    pointsX[0][16]=7.1; pointsY[0][16]=13.099998 ;
    pointsX[0][17]=6.9; pointsY[0][17]=12.5 ;
    pointsX[0][18]=6.4; pointsY[0][18]=12.0 ;
    pointsX[0][19]=5.6; pointsY[0][19]=11.599998 ;
    pointsX[0][20]=5.0; pointsY[0][20]=11.599998 ;
    pointsX[0][21]=4.4; pointsY[0][21]=11.599998 ;
    pointsX[0][22]=3.7; pointsY[0][22]=11.599998 ;
    pointsX[0][23]=3.1000001; pointsY[0][23]=11.700001 ;
    pointsX[0][24]=2.5; pointsY[0][24]=12.0 ;
    pointsX[0][25]=2.0; pointsY[0][25]=12.400002 ;
    pointsX[0][26]=1.5999999; pointsY[0][26]=12.900002 ;
    pointsX[0][27]=1.2; pointsY[0][27]=14.0 ;
    pointsX[0][28]=1.0999999; pointsY[0][28]=15.200001 ;
    pointsX[0][29]=1.0; pointsY[0][29]=16.5 ;
    pointsX[0][30]=1.0; pointsY[0][30]=17.300003 ;
    pointsX[0][31]=1.0; pointsY[0][31]=18.5 ;
    pointsX[0][32]=1.0; pointsY[0][32]=19.5 ;
    pointsX[0][33]=1.2; pointsY[0][33]=20.599998 ;
    pointsX[0][34]=1.4000001; pointsY[0][34]=21.800003 ;
    pointsX[0][35]=1.5999999; pointsY[0][35]=23.099998 ;
    pointsX[0][36]=1.8000002; pointsY[0][36]=23.900002 ;
    pointsX[0][37]=2.0; pointsY[0][37]=25.0 ;
    pointsX[0][38]=2.3; pointsY[0][38]=25.900002 ;
    pointsX[0][39]=2.6000001; pointsY[0][39]=26.800003 ;
    pointsX[0][40]=2.9; pointsY[0][40]=27.099998 ;
    pointsX[0][41]=3.2; pointsY[0][41]=27.300003 ;
    pointsX[0][42]=3.4; pointsY[0][42]=27.400002 ;
    pointsX[0][43]=3.5; pointsY[0][43]=27.5 ;
    pointsX[0][44]=3.7; pointsY[0][44]=27.599998 ;
    pointsX[0][45]=3.8; pointsY[0][45]=27.599998 ;
    pointsX[0][46]=3.8; pointsY[0][46]=27.599998 ;
    pointsX[0][47]=3.8; pointsY[0][47]=27.7 ;

    // number : 1
    pointsX[1][ 0 ]= 4.8 ; pointsY[1][ 0 ]= 34.699997 ;
    pointsX[1][ 1 ]= 4.8 ; pointsY[1][ 1 ]= 33.4 ;
    pointsX[1][ 2 ]= 4.8 ; pointsY[1][ 2 ]= 32.2 ;
    pointsX[1][ 3 ]= 4.9 ; pointsY[1][ 3 ]= 31.099998 ;
    pointsX[1][ 4 ]= 5.0 ; pointsY[1][ 4 ]= 29.7 ;
    pointsX[1][ 5 ]= 4.9 ; pointsY[1][ 5 ]= 27.800003 ;
    pointsX[1][ 6 ]= 4.4 ; pointsY[1][ 6 ]= 25.2 ;
    pointsX[1][ 7 ]= 4.2000003 ; pointsY[1][ 7 ]= 22.800003 ;
    pointsX[1][ 8 ]= 4.2000003 ; pointsY[1][ 8 ]= 20.599998 ;
    pointsX[1][ 9 ]= 4.2000003 ; pointsY[1][ 9 ]= 18.599998 ;
    pointsX[1][ 10 ]= 4.3 ; pointsY[1][ 10 ]= 17.0 ;
    pointsX[1][ 11 ]= 4.4 ; pointsY[1][ 11 ]= 14.599998 ;
    pointsX[1][ 12 ]= 4.4 ; pointsY[1][ 12 ]= 10.700001 ;
    pointsX[1][ 13 ]= 3.9 ; pointsY[1][ 13 ]= 8.0 ;
    pointsX[1][ 14 ]= 3.9 ; pointsY[1][ 14 ]= 6.0 ;
    pointsX[1][ 15 ]= 4.0 ; pointsY[1][ 15 ]= 4.799999 ;
    pointsX[1][ 16 ]= 4.1 ; pointsY[1][ 16 ]= 4.200001 ;
    pointsX[1][ 17 ]= 4.2000003 ; pointsY[1][ 17 ]= 3.9000015 ;
    pointsX[1][ 18 ]= 4.2000003 ; pointsY[1][ 18 ]= 3.9000015 ;
        
    //number : 2
    pointsX[2][0]=1.7; pointsY[2][0]=25.2 ;
    pointsX[2][1]=1.7; pointsY[2][1]=25.300003 ;
    pointsX[2][2]=1.7; pointsY[2][2]=26.099998 ;
    pointsX[2][3]=1.7; pointsY[2][3]=26.599998 ;
    pointsX[2][4]=2.2; pointsY[2][4]=27.800003 ;
    pointsX[2][5]=2.6000001; pointsY[2][5]=28.900002 ;
    pointsX[2][6]=3.3; pointsY[2][6]=30.0 ;
    pointsX[2][7]=4.1; pointsY[2][7]=31.0 ;
    pointsX[2][8]=4.8; pointsY[2][8]=31.300003 ;
    pointsX[2][9]=5.5; pointsY[2][9]=31.300003 ;
    pointsX[2][10]=6.1; pointsY[2][10]=31.0 ;
    pointsX[2][11]=7.0; pointsY[2][11]=30.300003 ;
    pointsX[2][12]=7.5; pointsY[2][12]=29.400002 ;
    pointsX[2][13]=7.6; pointsY[2][13]=28.099998 ;
    pointsX[2][14]=7.6; pointsY[2][14]=26.800003 ;
    pointsX[2][15]=7.1; pointsY[2][15]=25.2 ;
    pointsX[2][16]=6.3; pointsY[2][16]=23.7 ;
    pointsX[2][17]=5.4; pointsY[2][17]=22.400002 ;
    pointsX[2][18]=4.6; pointsY[2][18]=21.2 ;
    pointsX[2][19]=3.3; pointsY[2][19]=18.900002 ;
    pointsX[2][20]=2.3; pointsY[2][20]=17.2 ;
    pointsX[2][21]=1.4000001; pointsY[2][21]=15.599998 ;
    pointsX[2][22]=0.9000001; pointsY[2][22]=14.299999 ;
    pointsX[2][23]=0.8000002; pointsY[2][23]=13.200001 ;
    pointsX[2][24]=1.0; pointsY[2][24]=12.200001 ;
    pointsX[2][25]=1.3000002; pointsY[2][25]=11.700001 ;
    pointsX[2][26]=1.7; pointsY[2][26]=11.5 ;
    pointsX[2][27]=2.7; pointsY[2][27]=11.299999 ;
    pointsX[2][28]=3.8; pointsY[2][28]=11.400002 ;
    pointsX[2][29]=4.7000003; pointsY[2][29]=11.599998 ;
    pointsX[2][30]=5.4; pointsY[2][30]=11.799999 ;
    pointsX[2][31]=6.3; pointsY[2][31]=12.0 ;
    pointsX[2][32]=7.1; pointsY[2][32]=12.0 ;
    pointsX[2][33]=7.9; pointsY[2][33]=12.0 ;
    pointsX[2][34]=8.400001; pointsY[2][34]=12.0 ;
    pointsX[2][35]=8.6; pointsY[2][35]=12.0 ;
    pointsX[2][36]=8.900001; pointsY[2][36]=12.0 ;
    pointsX[2][37]=9.0; pointsY[2][37]=12.0 ;
    pointsX[2][38]=9.1; pointsY[2][38]=12.0 ;
    pointsX[2][39]=9.1; pointsY[2][39]=12.0 ;

    //number : 3
    pointsX[3][0]=1.0; pointsY[3][0]=30.0 ;
    pointsX[3][1]=1.0999999; pointsY[3][1]=30.800003 ;
    pointsX[3][2]=1.5999999; pointsY[3][2]=31.599998 ;
    pointsX[3][3]=2.5; pointsY[3][3]=32.4 ;
    pointsX[3][4]=3.7; pointsY[3][4]=32.9 ;
    pointsX[3][5]=4.7000003; pointsY[3][5]=33.0 ;
    pointsX[3][6]=5.7000003; pointsY[3][6]=32.4 ;
    pointsX[3][7]=6.6; pointsY[3][7]=31.2 ;
    pointsX[3][8]=7.3; pointsY[3][8]=30.0 ;
    pointsX[3][9]=7.7000003; pointsY[3][9]=28.400002 ;
    pointsX[3][10]=7.7000003; pointsY[3][10]=27.099998 ;
    pointsX[3][11]=7.3; pointsY[3][11]=25.599998 ;
    pointsX[3][12]=6.6; pointsY[3][12]=24.400002 ;
    pointsX[3][13]=5.5; pointsY[3][13]=23.099998 ;
    pointsX[3][14]=4.1; pointsY[3][14]=21.800003 ;
    pointsX[3][15]=2.8; pointsY[3][15]=20.7 ;
    pointsX[3][16]=1.7; pointsY[3][16]=20.0 ;
    pointsX[3][17]=1.2; pointsY[3][17]=19.800003 ;
    pointsX[3][18]=1.2; pointsY[3][18]=19.800003 ;
    pointsX[3][19]=1.8000002; pointsY[3][19]=20.099998 ;
    pointsX[3][20]=3.4; pointsY[3][20]=20.2 ;
    pointsX[3][21]=4.5; pointsY[3][21]=19.900002 ;
    pointsX[3][22]=5.4; pointsY[3][22]=19.400002 ;
    pointsX[3][23]=6.6; pointsY[3][23]=18.099998 ;
    pointsX[3][24]=7.8; pointsY[3][24]=16.599998 ;
    pointsX[3][25]=8.3; pointsY[3][25]=15.299999 ;
    pointsX[3][26]=8.5; pointsY[3][26]=13.900002 ;
    pointsX[3][27]=8.6; pointsY[3][27]=12.299999 ;
    pointsX[3][28]=8.6; pointsY[3][28]=11.0 ;
    pointsX[3][29]=8.3; pointsY[3][29]=9.700001 ;
    pointsX[3][30]=7.3; pointsY[3][30]=8.200001 ;
    pointsX[3][31]=5.9; pointsY[3][31]=6.799999 ;
    pointsX[3][32]=4.5; pointsY[3][32]=5.9000015 ;
    pointsX[3][33]=3.1000001; pointsY[3][33]=5.4000015 ;
    pointsX[3][34]=2.0; pointsY[3][34]=5.200001 ;
    pointsX[3][35]=1.5999999; pointsY[3][35]=5.0999985 ;
    pointsX[3][36]=1.5999999; pointsY[3][36]=5.0999985 ;
    pointsX[3][37]=1.5999999; pointsY[3][37]=5.0999985 ;
    
    //number : 4
    pointsX[4][0]=9.1; pointsY[4][0]=16.7 ;
    pointsX[4][1]=8.0; pointsY[4][1]=16.7 ;
    pointsX[4][2]=6.8; pointsY[4][2]=16.7 ;
    pointsX[4][3]=5.5; pointsY[4][3]=16.800003 ;
    pointsX[4][4]=4.1; pointsY[4][4]=16.7 ;
    pointsX[4][5]=3.3; pointsY[4][5]=16.599998 ;
    pointsX[4][6]=2.6000001; pointsY[4][6]=16.599998 ;
    pointsX[4][7]=1.7; pointsY[4][7]=16.599998 ;
    pointsX[4][8]=1.2; pointsY[4][8]=16.599998 ;
    pointsX[4][9]=0.8000002; pointsY[4][9]=16.599998 ;
    pointsX[4][10]=0.4000001; pointsY[4][10]=16.599998 ;
    pointsX[4][11]=0.4000001; pointsY[4][11]=16.7 ;
    pointsX[4][12]=0.9000001; pointsY[4][12]=18.099998 ;
    pointsX[4][13]=1.5999999; pointsY[4][13]=19.400002 ;
    pointsX[4][14]=2.1000001; pointsY[4][14]=20.599998 ;
    pointsX[4][15]=2.5; pointsY[4][15]=22.0 ;
    pointsX[4][16]=2.9; pointsY[4][16]=23.099998 ;
    pointsX[4][17]=3.2; pointsY[4][17]=24.300003 ;
    pointsX[4][18]=3.5; pointsY[4][18]=25.599998 ;
    pointsX[4][19]=4.0; pointsY[4][19]=26.900002 ;
    pointsX[4][20]=5.1; pointsY[4][20]=29.2 ;
    pointsX[4][21]=5.7000003; pointsY[4][21]=30.599998 ;
    pointsX[4][22]=6.4; pointsY[4][22]=32.0 ;
    pointsX[4][23]=6.4; pointsY[4][23]=32.2 ;
    pointsX[4][24]=6.4; pointsY[4][24]=32.2 ;
    pointsX[4][25]=7.0; pointsY[4][25]=31.300003 ;
    pointsX[4][26]=7.0; pointsY[4][26]=30.300003 ;
    pointsX[4][27]=7.0; pointsY[4][27]=29.300003 ;
    pointsX[4][28]=6.9; pointsY[4][28]=28.099998 ;
    pointsX[4][29]=6.9; pointsY[4][29]=26.800003 ;
    pointsX[4][30]=6.9; pointsY[4][30]=25.5 ;
    pointsX[4][31]=7.0; pointsY[4][31]=24.2 ;
    pointsX[4][32]=7.2000003; pointsY[4][32]=23.0 ;
    pointsX[4][33]=7.3; pointsY[4][33]=21.900002 ;
    pointsX[4][34]=7.3; pointsY[4][34]=20.7 ;
    pointsX[4][35]=7.3; pointsY[4][35]=19.300003 ;
    pointsX[4][36]=7.2000003; pointsY[4][36]=18.0 ;
    pointsX[4][37]=7.1; pointsY[4][37]=16.599998 ;
    pointsX[4][38]=7.2000003; pointsY[4][38]=15.200001 ;
    pointsX[4][39]=7.2000003; pointsY[4][39]=13.799999 ;
    pointsX[4][40]=7.5; pointsY[4][40]=12.400002 ;
    pointsX[4][41]=7.6; pointsY[4][41]=11.099998 ;
    pointsX[4][42]=7.8; pointsY[4][42]=9.700001 ;
    pointsX[4][43]=7.8; pointsY[4][43]=8.400002 ;
    pointsX[4][44]=7.4; pointsY[4][44]=7.0999985 ;
    pointsX[4][45]=7.1; pointsY[4][45]=5.9000015 ;
    pointsX[4][46]=7.0; pointsY[4][46]=4.9000015 ;
    pointsX[4][47]=7.0; pointsY[4][47]=4.9000015 ;
    //number : 5
    pointsX[5][0]=7.9; pointsY[5][0]=32.8 ;
    pointsX[5][1]=6.5; pointsY[5][1]=32.5 ;
    pointsX[5][2]=5.3; pointsY[5][2]=32.2 ;
    pointsX[5][3]=4.1; pointsY[5][3]=31.900002 ;
    pointsX[5][4]=3.6000001; pointsY[5][4]=31.900002 ;
    pointsX[5][5]=3.0; pointsY[5][5]=31.900002 ;
    pointsX[5][6]=2.2; pointsY[5][6]=31.900002 ;
    pointsX[5][7]=1.9000001; pointsY[5][7]=31.900002 ;
    pointsX[5][8]=1.7; pointsY[5][8]=32.0 ;
    pointsX[5][9]=1.7; pointsY[5][9]=32.0 ;
    pointsX[5][10]=1.4000001; pointsY[5][10]=30.800003 ;
    pointsX[5][11]=1.4000001; pointsY[5][11]=29.7 ;
    pointsX[5][12]=1.4000001; pointsY[5][12]=28.400002 ;
    pointsX[5][13]=1.4000001; pointsY[5][13]=27.300003 ;
    pointsX[5][14]=1.4000001; pointsY[5][14]=26.2 ;
    pointsX[5][15]=1.4000001; pointsY[5][15]=24.900002 ;
    pointsX[5][16]=1.5; pointsY[5][16]=23.800003 ;
    pointsX[5][17]=1.5999999; pointsY[5][17]=22.7 ;
    pointsX[5][18]=1.5999999; pointsY[5][18]=21.599998 ;
    pointsX[5][19]=1.5999999; pointsY[5][19]=20.7 ;
    pointsX[5][20]=1.5999999; pointsY[5][20]=20.5 ;
    pointsX[5][21]=1.5999999; pointsY[5][21]=20.5 ;
    pointsX[5][22]=2.4; pointsY[5][22]=21.300003 ;
    pointsX[5][23]=3.6000001; pointsY[5][23]=21.599998 ;
    pointsX[5][24]=4.4; pointsY[5][24]=21.599998 ;
    pointsX[5][25]=5.4; pointsY[5][25]=21.300003 ;
    pointsX[5][26]=5.9; pointsY[5][26]=20.599998 ;
    pointsX[5][27]=6.5; pointsY[5][27]=19.300003 ;
    pointsX[5][28]=7.2000003; pointsY[5][28]=17.900002 ;
    pointsX[5][29]=7.7000003; pointsY[5][29]=16.5 ;
    pointsX[5][30]=8.0; pointsY[5][30]=15.200001 ;
    pointsX[5][31]=8.2; pointsY[5][31]=13.599998 ;
    pointsX[5][32]=7.8; pointsY[5][32]=11.5 ;
    pointsX[5][33]=6.8; pointsY[5][33]=10.200001 ;
    pointsX[5][34]=5.6; pointsY[5][34]=8.799999 ;
    pointsX[5][35]=4.2000003; pointsY[5][35]=7.299999 ;
    pointsX[5][36]=3.0; pointsY[5][36]=6.299999 ;
    pointsX[5][37]=2.1000001; pointsY[5][37]=5.4000015 ;
    pointsX[5][38]=2.0; pointsY[5][38]=5.299999 ;
    pointsX[5][39]=2.0; pointsY[5][39]=5.299999 ;
    pointsX[5][40]=2.0; pointsY[5][40]=5.299999 ;
    
    //number : 6
    pointsX[6][0]=5.7000003; pointsY[6][0]=33.5 ;
    pointsX[6][1]=5.1; pointsY[6][1]=32.8 ;
    pointsX[6][2]=4.6; pointsY[6][2]=31.900002 ;
    pointsX[6][3]=4.0; pointsY[6][3]=30.800003 ;
    pointsX[6][4]=3.6000001; pointsY[6][4]=29.5 ;
    pointsX[6][5]=3.4; pointsY[6][5]=28.300003 ;
    pointsX[6][6]=3.2; pointsY[6][6]=27.400002 ;
    pointsX[6][7]=2.9; pointsY[6][7]=26.2 ;
    pointsX[6][8]=2.6000001; pointsY[6][8]=25.0 ;
    pointsX[6][9]=2.1000001; pointsY[6][9]=23.599998 ;
    pointsX[6][10]=1.5999999; pointsY[6][10]=22.300003 ;
    pointsX[6][11]=1.2; pointsY[6][11]=20.900002 ;
    pointsX[6][12]=1.0999999; pointsY[6][12]=19.599998 ;
    pointsX[6][13]=1.0999999; pointsY[6][13]=18.300003 ;
    pointsX[6][14]=1.0999999; pointsY[6][14]=17.099998 ;
    pointsX[6][15]=1.0999999; pointsY[6][15]=15.799999 ;
    pointsX[6][16]=1.0999999; pointsY[6][16]=14.299999 ;
    pointsX[6][17]=1.2; pointsY[6][17]=12.599998 ;
    pointsX[6][18]=1.7; pointsY[6][18]=10.900002 ;
    pointsX[6][19]=2.5; pointsY[6][19]=9.599998 ;
    pointsX[6][20]=3.8; pointsY[6][20]=8.299999 ;
    pointsX[6][21]=4.8; pointsY[6][21]=8.099998 ;
    pointsX[6][22]=6.0; pointsY[6][22]=8.799999 ;
    pointsX[6][23]=6.7000003; pointsY[6][23]=9.900002 ;
    pointsX[6][24]=7.3; pointsY[6][24]=11.099998 ;
    pointsX[6][25]=7.6; pointsY[6][25]=12.400002 ;
    pointsX[6][26]=7.8; pointsY[6][26]=13.799999 ;
    pointsX[6][27]=7.9; pointsY[6][27]=15.299999 ;
    pointsX[6][28]=7.8; pointsY[6][28]=16.599998 ;
    pointsX[6][29]=7.4; pointsY[6][29]=17.7 ;
    pointsX[6][30]=6.7000003; pointsY[6][30]=18.900002 ;
    pointsX[6][31]=5.9; pointsY[6][31]=20.099998 ;
    pointsX[6][32]=5.1; pointsY[6][32]=20.900002 ;
    pointsX[6][33]=4.1; pointsY[6][33]=21.2 ;
    pointsX[6][34]=3.2; pointsY[6][34]=21.2 ;
    pointsX[6][35]=2.6000001; pointsY[6][35]=21.2 ;
    pointsX[6][36]=1.9000001; pointsY[6][36]=20.900002 ;
    pointsX[6][37]=1.5999999; pointsY[6][37]=20.7 ;
    pointsX[6][38]=1.3000002; pointsY[6][38]=20.400002 ;
    pointsX[6][39]=1.3000002; pointsY[6][39]=20.300003 ;
    
    //number : 7
    pointsX[7][0]=1.8000002; pointsY[7][0]=27.099998 ;
    pointsX[7][1]=1.5999999; pointsY[7][1]=28.0 ;
    pointsX[7][2]=1.5999999; pointsY[7][2]=29.099998 ;
    pointsX[7][3]=1.5; pointsY[7][3]=30.2 ;
    pointsX[7][4]=1.5999999; pointsY[7][4]=31.300003 ;
    pointsX[7][5]=1.8000002; pointsY[7][5]=31.900002 ;
    pointsX[7][6]=1.8000002; pointsY[7][6]=32.0 ;
    pointsX[7][7]=1.8000002; pointsY[7][7]=32.0 ;
    pointsX[7][8]=2.2; pointsY[7][8]=32.0 ;
    pointsX[7][9]=3.6000001; pointsY[7][9]=32.0 ;
    pointsX[7][10]=4.6; pointsY[7][10]=32.100002 ;
    pointsX[7][11]=5.5; pointsY[7][11]=32.2 ;
    pointsX[7][12]=6.1; pointsY[7][12]=32.4 ;
    pointsX[7][13]=6.6; pointsY[7][13]=32.4 ;
    pointsX[7][14]=6.8; pointsY[7][14]=32.5 ;
    pointsX[7][15]=6.8; pointsY[7][15]=32.5 ;
    pointsX[7][16]=7.1; pointsY[7][16]=31.5 ;
    pointsX[7][17]=7.1; pointsY[7][17]=30.099998 ;
    pointsX[7][18]=6.8; pointsY[7][18]=28.800003 ;
    pointsX[7][19]=6.4; pointsY[7][19]=27.300003 ;
    pointsX[7][20]=6.1; pointsY[7][20]=25.900002 ;
    pointsX[7][21]=6.0; pointsY[7][21]=24.0 ;
    pointsX[7][22]=5.9; pointsY[7][22]=22.300003 ;
    pointsX[7][23]=5.6; pointsY[7][23]=21.0 ;
    pointsX[7][24]=5.1; pointsY[7][24]=19.300003 ;
    pointsX[7][25]=4.9; pointsY[7][25]=17.900002 ;
    pointsX[7][26]=4.6; pointsY[7][26]=16.0 ;
    pointsX[7][27]=4.6; pointsY[7][27]=13.900002 ;
    pointsX[7][28]=4.3; pointsY[7][28]=12.299999 ;
    pointsX[7][29]=3.9; pointsY[7][29]=10.700001 ;
    pointsX[7][30]=3.7; pointsY[7][30]=9.5 ;
    pointsX[7][31]=3.5; pointsY[7][31]=8.200001 ;
    pointsX[7][32]=3.5; pointsY[7][32]=7.5 ;
    pointsX[7][33]=3.5; pointsY[7][33]=7.4000015 ;
    pointsX[7][34]=3.5; pointsY[7][34]=7.4000015 ;
    //number : 8
    pointsX[8][0]=5.0; pointsY[8][0]=21.099998 ;
    pointsX[8][1]=4.5; pointsY[8][1]=21.5 ;
    pointsX[8][2]=3.9; pointsY[8][2]=21.900002 ;
    pointsX[8][3]=3.4; pointsY[8][3]=22.300003 ;
    pointsX[8][4]=3.0; pointsY[8][4]=22.900002 ;
    pointsX[8][5]=2.4; pointsY[8][5]=24.0 ;
    pointsX[8][6]=1.9000001; pointsY[8][6]=25.300003 ;
    pointsX[8][7]=1.7; pointsY[8][7]=26.7 ;
    pointsX[8][8]=1.7; pointsY[8][8]=27.900002 ;
    pointsX[8][9]=1.9000001; pointsY[8][9]=29.300003 ;
    pointsX[8][10]=2.6000001; pointsY[8][10]=30.599998 ;
    pointsX[8][11]=3.7; pointsY[8][11]=31.900002 ;
    pointsX[8][12]=4.8; pointsY[8][12]=32.7 ;
    pointsX[8][13]=6.0; pointsY[8][13]=32.600002 ;
    pointsX[8][14]=7.2000003; pointsY[8][14]=31.599998 ;
    pointsX[8][15]=8.0; pointsY[8][15]=30.400002 ;
    pointsX[8][16]=8.3; pointsY[8][16]=28.900002 ;
    pointsX[8][17]=8.400001; pointsY[8][17]=27.599998 ;
    pointsX[8][18]=8.3; pointsY[8][18]=26.400002 ;
    pointsX[8][19]=8.1; pointsY[8][19]=25.099998 ;
    pointsX[8][20]=7.6; pointsY[8][20]=23.800003 ;
    pointsX[8][21]=7.1; pointsY[8][21]=22.599998 ;
    pointsX[8][22]=6.5; pointsY[8][22]=21.400002 ;
    pointsX[8][23]=5.9; pointsY[8][23]=20.5 ;
    pointsX[8][24]=5.0; pointsY[8][24]=19.300003 ;
    pointsX[8][25]=4.0; pointsY[8][25]=18.2 ;
    pointsX[8][26]=3.2; pointsY[8][26]=16.900002 ;
    pointsX[8][27]=2.4; pointsY[8][27]=15.5 ;
    pointsX[8][28]=2.0; pointsY[8][28]=13.400002 ;
    pointsX[8][29]=2.5; pointsY[8][29]=11.700001 ;
    pointsX[8][30]=3.7; pointsY[8][30]=10.200001 ;
    pointsX[8][31]=5.0; pointsY[8][31]=8.799999 ;
    pointsX[8][32]=6.2000003; pointsY[8][32]=8.700001 ;
    pointsX[8][33]=7.3; pointsY[8][33]=9.400002 ;
    pointsX[8][34]=8.400001; pointsY[8][34]=10.700001 ;
    pointsX[8][35]=8.7; pointsY[8][35]=12.099998 ;
    pointsX[8][36]=8.7; pointsY[8][36]=13.400002 ;
    pointsX[8][37]=8.6; pointsY[8][37]=14.799999 ;
    pointsX[8][38]=8.2; pointsY[8][38]=16.099998 ;
    pointsX[8][39]=7.6; pointsY[8][39]=17.7 ;
    pointsX[8][40]=6.8; pointsY[8][40]=19.0 ;
    pointsX[8][41]=6.0; pointsY[8][41]=20.300003 ;
    pointsX[8][42]=5.7000003; pointsY[8][42]=20.800003 ;
    pointsX[8][43]=5.6; pointsY[8][43]=21.0 ;
    
    //number : 9
    pointsX[9][0]=8.0; pointsY[9][0]=24.900002 ;
    pointsX[9][1]=8.0; pointsY[9][1]=24.900002 ;
    pointsX[9][2]=7.4; pointsY[9][2]=24.2 ;
    pointsX[9][3]=6.4; pointsY[9][3]=23.5 ;
    pointsX[9][4]=5.4; pointsY[9][4]=23.400002 ;
    pointsX[9][5]=4.3; pointsY[9][5]=23.400002 ;
    pointsX[9][6]=3.7; pointsY[9][6]=23.599998 ;
    pointsX[9][7]=3.1000001; pointsY[9][7]=24.400002 ;
    pointsX[9][8]=2.7; pointsY[9][8]=25.5 ;
    pointsX[9][9]=2.5; pointsY[9][9]=26.900002 ;
    pointsX[9][10]=2.5; pointsY[9][10]=28.599998 ;
    pointsX[9][11]=3.2; pointsY[9][11]=30.099998 ;
    pointsX[9][12]=4.3; pointsY[9][12]=31.599998 ;
    pointsX[9][13]=5.5; pointsY[9][13]=32.5 ;
    pointsX[9][14]=6.7000003; pointsY[9][14]=32.5 ;
    pointsX[9][15]=7.6; pointsY[9][15]=31.800003 ;
    pointsX[9][16]=8.3; pointsY[9][16]=30.7 ;
    pointsX[9][17]=8.8; pointsY[9][17]=29.400002 ;
    pointsX[9][18]=9.0; pointsY[9][18]=28.099998 ;
    pointsX[9][19]=9.0; pointsY[9][19]=26.900002 ;
    pointsX[9][20]=9.0; pointsY[9][20]=25.599998 ;
    pointsX[9][21]=8.900001; pointsY[9][21]=24.300003 ;
    pointsX[9][22]=8.8; pointsY[9][22]=22.900002 ;
    pointsX[9][23]=8.3; pointsY[9][23]=21.099998 ;
    pointsX[9][24]=7.5; pointsY[9][24]=19.2 ;
    pointsX[9][25]=6.6; pointsY[9][25]=17.400002 ;
    pointsX[9][26]=5.9; pointsY[9][26]=15.299999 ;
    pointsX[9][27]=5.4; pointsY[9][27]=13.799999 ;
    pointsX[9][28]=4.8; pointsY[9][28]=12.5 ;
    pointsX[9][29]=4.1; pointsY[9][29]=11.200001 ;
    pointsX[9][30]=3.4; pointsY[9][30]=9.799999 ;
    pointsX[9][31]=3.1000001; pointsY[9][31]=8.799999 ;
    pointsX[9][32]=2.7; pointsY[9][32]=7.9000015 ;
    pointsX[9][33]=2.4; pointsY[9][33]=7.0999985 ;
    pointsX[9][34]=2.4; pointsY[9][34]=7.0999985 ;

  } // 생성자
} //class